#!/bin/sh
#
#a script that sets the screen brightness to a certain procentage

path="/sys/class/backlight/intel_backlight"
entered_brighness=$(rofi -dmenu -p "enter desired brightness")
max_brightness=$(cat "$path/max_brightness")
desired_brightness_procantage=$(echo  "scale=1;"$entered_brighness" / 100" | bc)
desired_brightness=$(echo $max_brightness" * "$desired_brightness_procantage | bc | sed 's/\...*$//')
echo "$desired_brightness" > "$path"/brightness
