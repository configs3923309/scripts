#!/bin/sh
#
#asks for the user's password so it can run the real script as root since it needs root privilages to set the brightness

passphrase=$(rofi -dmenu -p "enter password")
echo "$passphrase" | sudo -S /home/ant/scripts//root-needed/set-brightness-root.sh
