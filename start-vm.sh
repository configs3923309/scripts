#!/bin/sh
#
#a script that starts a virtualbox virtual machine

path="/home/ant/virtualbox-vms"
run=$(ls $path | rofi -dmenu -p "start VM")
[ -z "$run" ] || VirtualBoxVM --startvm "$run"
