#!/bin/sh
#
#a script that takes a screenshot and copies text from it to clipboard
#requires scrot and tesseract installed

path="/home/$USER/Pictures/Screenshots" #i think it could be better to termporaily store them in /tmp but im not sure
cd $path
scrot -s -f -d 1 "to-be-removed" #takes the screenshot
tesseract "to-be-removed" "output-text" #gets the text from screenshot
cat "output-text.txt" | xclip -sel clip #copies the text from screenshot
rm "to-be-removed" #removes not needed files
rm "output-text.txt" #removes not needed files

