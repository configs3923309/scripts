#!/bin/sh
#
#a simple shell script that send a notification with battery capacity

capacity=$(cat /sys/class/power_supply/BAT0/capacity)
notify-send "battery is $capacity% full"
