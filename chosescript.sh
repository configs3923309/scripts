#!/bin/sh
#
#a script that lists other scripts in rofi so they can be easily run

location="/home/$USER/scripts"
script=$(ls $location | grep -E "\.sh" | sed 's/\.sh.*$//'| rofi -dmenu -p "run script")
$location/"$script.sh"
