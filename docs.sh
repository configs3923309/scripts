#!/bin/sh
#
#a script to easily access offline documentation
#the locations of documentation are stored in a external file called "docs.txt"

path="/home/$USER/scripts/docs.txt"

Chosenfile=$(awk '/^[^#]/ {print $1}' "$path" | rofi -dmenu -p "Configure") #lets the user chose what program to configre by name
Chosenpath=$(awk -v name="$Chosenfile" 'name ~ $1 {print $2}' "$path")

xdg-open /home/$USER$Chosenpath

