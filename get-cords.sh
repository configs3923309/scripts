#!/bin/bash
#
#a shell script for a game

file="secret" #set path to your cord file in the quotes

#filteres out invalid formatting and gives the option to chose place by name
selectedname=$(awk -F ":" '/^[^:]+:.*/ {print $1}' $file | rofi -dmenu -p "get cords")
[ "$selectedname" != "" ] && notify-send "copied $selectedname's cords to clipboard"; awk -F ":" -v pattern="$selectedname" 'pattern ~ $1  {print $2}' $file | xclip -sel clip #finds the cords of place that has a matching name and copies it to clipboard
