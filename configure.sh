#!/bin/sh
#
#a script to quickly access my config files via rofi
#the paths to config files are stored in a external file (by default its in the same directory as this script and is named "locations")

File="/home/$USER/scripts/locations" #set the path to your file here
Chosen=$(awk '/^[^#]/ {print $1}' "$File" | rofi -dmenu -p "configure")
Chosenpath=$(awk -v name="$Chosen" 'name ~ $1 {print $2}' "$File")
Isroot=$(awk -v name="$Chosen" 'name ~ $1 {print $3}' "$File")  #dirty way to check whether the file needs root, maybe i could use chmod?

[ "$Isroot" = "true"] && alacritty -e sudo vim "$Chosenpath" || alacritty -e  vim "$Chosenpath" #opens a terminal to edit the configuration file, asks for password if root access is needed
