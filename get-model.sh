#!/bin/sh

#shell script that allows the user to select a model by file name and copies the contents of that file to the clipboard

#path to the directory you store your models at
path="/home/ant/Documents/wos-stuffs/models/" 

selected=$(ls "$path" | rofi -dmenu -i -p "choose mode;")
[ -z "$selected" ] || xclip -sel clip "$path$selected"

