#!/bin/sh
#
#a script for searching the arch wiki offline
#prompts the user for a query via rofi

query=$(rofi -dmenu -p "Query")
alacritty -e wiki-search-html $query
